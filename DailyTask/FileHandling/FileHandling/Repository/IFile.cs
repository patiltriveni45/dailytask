﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal interface IFile
    {
        void WriteContentToFile(User user, string filename);
        List<string> ReadContentFile(string filename);
        bool IsUserNameExists(string username, string filename);
    }
}
