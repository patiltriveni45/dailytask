﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class Userrepository : IUserrepository, IFile
    {
        List<User> users;


        public Userrepository()
        {
            users = new List<User>();
        }
        public bool IsUserNameExists(string username, string filename)
        {
            bool isUseravilable = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowline;
                while ((rowline = sr.ReadLine()) != null)
                {
                    string[] rowssplitted = rowline.Split(',');
                    foreach (string item in rowssplitted)
                    {
                        if (item == username)
                        {
                            isUseravilable = true;
                            throw new Invaliduser("Username already exist ");
                            break;
                        }
                    }
                }
                return isUseravilable;
            }

        }

        public List<string> ReadContentFile(string filename)
        {
            List<string> filecontent = new List<string>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowline;
                while ((rowline = sr.ReadLine()) != null)
                {
                    filecontent.Add(rowline);
                }
            }
            return filecontent;
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string filename = "user1.txt";
            bool result = IsUserNameExists(user.Name, filename);
            if (result)
            {
                return false;
            }
            else
            {
                WriteContentToFile(user, filename);
                return true;
            }

        }

        public void WriteContentToFile(User user, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename, true))
            {
                sw.Write($"{user}");
            }
        }
    }
}
